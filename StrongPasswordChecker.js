function strongPasswordChecker(string) {
    var result = 0;
    var lower = 1,
        upper = 1, repeat = 0,
        length = 0;
    number = 1;
    var carr = [];
    carr = Array.from(string);

    for (let i = 0; i < carr.length - 1; i++) {
        if (carr[i] == carr[i].toLowerCase()) lower = 0;
        if (carr[i] == carr[i].toUpperCase()) upper = 0;
        if (typeof (carr[i]) == 'number') number = 0;

        if (i != 0 && i != carr.length - 1) {
            if ((carr[i] == carr[i - 1]) && (carr[i] == carr[i + 1]))
                repeat = 1;
        }
    }

    if (carr.length < 6 || carr.length > 20) length = 1;

    let missingRules = (lower + upper + number + repeat + length);

    console.log("Missing Rule " + missingRules);
    return missingRules;
}
