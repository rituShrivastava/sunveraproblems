function MedianOfSortedArray(arr, arrLen) {
    if (arrLen % 2 == 0) {
        var index1 = arrLen / 2;
        var median1 = arr[index1];
        var median2 = arr[index1 - 1];
        var ans = (median1 + median2) / 2;
        return ans;
    }
    else {
        var index1 = Math.floor(arrLen / 2);
        return arr[index1];
    }
}

//var arr1 = [1, 2, 3];
//var arr2 = [4, 5];

var arr1 = [-1,0,1,2,3,4,7];
var arr2 = [2,3,4,6,9,10,11,12];

var i = arr1.length;
var j = arr2.length;

var l = i + j;

const arr3 = arr1.concat(arr2);

arr3.sort(function (a1, a2) {
    return a1 - a2;
});

console.log("Median = " + MedianOfSortedArray(arr3, l));

